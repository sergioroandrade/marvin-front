#!/bin/bash
# Creates boilerplate for React Component

compontentName=${1^}
fileName=$1.component.jsx
cd "./src/components"
mkdir $1
cd "$1"
touch ${fileName}
touch $1.module.scss
echo "@import 'styles/variables';" >> $1.module.scss
echo "import styles from './$1.module.scss';" >> ${fileName}
echo "" >> ${fileName}
echo "export function ${compontentName}() {" >> ${fileName}
echo "  return <h1>${compontentName}</h1>;" >> ${fileName}
echo "}" >> ${fileName}
cd "../"
echo "export { ${compontentName} } from './${1}/${1}.component';" >> index.jsx