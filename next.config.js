module.exports = {
  images: {
    domains: ['res.cloudinary.com'],
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(png|jpg|gif)$/i,
      use: [
        {
          loader: 'url-loader',
          options: {
            mimetype: 'image/png',
          },
        },
      ],
    });
    return config;
  },
};
