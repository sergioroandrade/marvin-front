import Head from 'next/head';

export function Analytics({ code }) {
  if (!code) {
    return;
  }
  return (
    <>
      <Head>
        <div
          dangerouslySetInnerHTML={{
            __html: `${code}`,
          }}
        />
      </Head>
    </>
  );
}
