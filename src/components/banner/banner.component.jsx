import { ButtonWithArrow } from 'components';
import Image from 'next/image';
import banner from './banner.module.scss';

export function Banner({ data }) {
  const style = {
    backgroundImage: `url(${data?.image?.url})`,
  };

  return (
    <section className={banner.wrapper} style={style}>
      <div className={banner.container}>
        <div className={banner.content}>
          <Image
            src='/images/slogan.svg'
            height={200}
            width={400}
            alt='Todo vinho conta uma história'
          />
          <ButtonWithArrow className={banner.button} href={data?.link?.url}>
            {data?.link?.text}
          </ButtonWithArrow>
        </div>
      </div>
    </section>
  );
}
