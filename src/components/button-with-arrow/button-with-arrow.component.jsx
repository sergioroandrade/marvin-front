import { ArrowRightIcon } from 'icons';
import Link from 'next/link';
import styles from './button-with-arrow.module.scss';

export function ButtonWithArrow({ href, children, className = '' }) {
  return (
    <div className={`${styles.wrapper} ${className}`}>
      <ArrowRightIcon className={styles.icon} />
      <span className={styles.text}>
        <Link href={href}>{children}</Link>
      </span>
    </div>
  );
}
