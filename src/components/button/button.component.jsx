import Link from 'next/link';
import button from './button.module.scss';

export function Button({ href, to, variant, children, className, ...rest }) {
  const variations = {
    primary: button.primary,
    secondary: button.secondary,
    tertiary: button.tertiary,
    quarternary: button.quarternary,
  };
  const currentVariant = variations[variant] || button.primary;
  if (href) {
    return (
      <a
        href={href}
        className={`${button.button} ${
          className ? className : ''
        } ${currentVariant}`}
        {...rest}
      >
        {children}
      </a>
    );
  }

  if (to) {
    return (
      <Link href={to}>
        <div
          className={`${button.button} ${
            className ? className : ''
          } ${currentVariant}`}
          {...rest}
        >
          {children}
        </div>
      </Link>
    );
  }

  return (
    <button
      className={`${button.button} ${className} ${currentVariant}`}
      {...rest}
    >
      {children}
    </button>
  );
}
