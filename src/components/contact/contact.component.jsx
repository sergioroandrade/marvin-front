import { Button, Input, Title } from 'components';
import { WhatsAppIcon } from 'icons';
import { useState } from 'react';
import contact from './contact.module.scss';

export function Contact({ phone }) {
  const [message, setMessage] = useState('');

  function sendForm() {
    const template = `*Contato via site:* ${message}`;
    window.open(
      `https://api.whatsapp.com/send?phone=${phone}&text=${template}`
    );
  }

  function handleSubmit(event) {
    event.preventDefault();
    sendForm();
  }

  function handleChangeMessage(event) {
    setMessage(event.target.value);
  }
  return (
    <section className={contact.wrapper} id='contato'>
      <div className={contact.container}>
        <Title hasRibbon={true} tag='h6'>
          Entre em Contato
        </Title>
        <form className={contact.form} onSubmit={handleSubmit}>
          <Input
            type='textarea'
            label='Mensagem'
            placeholder='Digite sua mensagem'
            required={true}
            onChange={(event) => handleChangeMessage(event)}
            value={message}
          />
          <Button className={contact.send}>
            <WhatsAppIcon className={contact.whatsapp} />
            Enviar mensagem
          </Button>
        </form>
      </div>
    </section>
  );
}
