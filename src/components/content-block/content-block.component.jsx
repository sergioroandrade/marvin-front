import { ButtonWithArrow, Small, Specs, Title } from 'components';
import Image from 'next/image';
import ReactHtmlParser from 'react-html-parser';
import content from './content-block.module.scss';

export function ContentBlock({
  headline,
  title,
  text,
  image,
  imageWidth = 500,
  imageHeight = 500,
  link,
  inverse,
  wine,
  className = '',
}) {
  return (
    <section className={`${content.container} ${className}`}>
      <div className={`${content.row} ${inverse ? content.inverse : ''}`}>
        <div className={content.column}>
          <div>
            <Small>{headline}</Small>
            <Title variant='wine' tag='h2'>
              {title}
            </Title>

            {ReactHtmlParser(text)}
            {wine && <Specs wine={wine} />}
            {link && (
              <ButtonWithArrow href={link.href}>{link.text}</ButtonWithArrow>
            )}
          </div>
        </div>
        <div className={content.column}>
          {image && (
            <Image
              src={image}
              width={imageWidth}
              height={imageHeight}
              objectFit='cover'
              alt={title}
            />
          )}
        </div>
      </div>
    </section>
  );
}
