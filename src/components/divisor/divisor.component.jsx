import { RibbonIcon } from 'icons';
import divisor from './divisor.module.scss';

export function Divisor() {
  return (
    <div className={divisor.wrapper}>
      <RibbonIcon />
    </div>
  );
}
