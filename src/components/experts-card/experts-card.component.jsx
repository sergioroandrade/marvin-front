import { BaseCard } from 'components';
import styles from './experts-card.module.scss';

export function ExpertsCard() {
  return (
    <div>
      <BaseCard
        title='Curso Lorem Ipsun'
        text='Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe quo ipsum error officiis quia minus, maiores, architecto quas rerum quidem eveniet eius, distinctio et! Quae minima magnam quos blanditiis commodi.'
      />
    </div>
  );
}
