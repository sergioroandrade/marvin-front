import Image from 'next/image';
import flag from './flag.module.scss';

const flagTranslation = [
  { name: 'África do Sul', filename: 'africa_do_sul' },
  { name: 'Alemanha', filename: 'alemanha' },
  { name: 'Argentina', filename: 'argentina' },
  { name: 'Austrália', filename: 'australia' },
  { name: 'Áustria', filename: 'austria' },
  { name: 'Brasil', filename: 'brasil' },
  { name: 'Chile', filename: 'chile' },
  { name: 'Espanha', filename: 'espanha' },
  { name: 'Estados Unidos', filename: 'estados_unidos' },
  { name: 'França', filename: 'franca' },
  { name: 'Hungria', filename: 'hungria' },
  { name: 'Itália', filename: 'italia' },
  { name: 'Líbano', filename: 'libano' },
  { name: 'Nova Zelândia', filename: 'nova_zelandia' },
  { name: 'Portugal', filename: 'portugal' },
  { name: 'Uruguai', filename: 'uruguai' },
  { name: 'Grécia', filename: 'grecia' },
];

export function Flag({ country, className = '' }) {
  function renderFlag(country) {
    const flag = flagTranslation.filter((flag) => flag.name === country);
    return (
      <Image
        src={`/flags/${flag[0].filename}.svg`}
        alt={flag[0].name}
        title={flag[0].name}
        width={60}
        height={60}
        objectFit='cover'
      />
    );
  }
  return (
    <div className={`${flag.wrapper} ${className}`}>
      {country && renderFlag(country)}
    </div>
  );
}
