import { Social } from 'components';
import Image from 'next/image';
import Link from 'next/link';
import footer from './footer.module.scss';

export function Footer({ data }) {
  return (
    <footer className={footer.wrapper}>
      <div className={footer.container}>
        <Image
          className={footer.logo}
          src='/images/marvin.png'
          width={200}
          height={100}
          objectFit='contain'
          alt='Marvin Importadora'
        />
        <ul className={footer.menu}>
          <li className={footer.link}>
            <Link href='/sobre'>Sobre</Link>
          </li>
          <li className={footer.link}>
            <Link href='/#destaques'>Destaques</Link>
          </li>
          <li className={footer.link}>
            <Link href='/projetos'>Produtores</Link>
          </li>
          <li className={footer.link}>
            <Link href='/origem'>Origem</Link>
          </li>
          <li className={footer.link}>
            <Link href='#contato'>Contato</Link>
          </li>
        </ul>
        <Social
          facebook={data?.facebook}
          instagram={data?.instagram}
          className={footer.social}
        />
        <div className={footer.rights}>
          <div>{data?.address}</div>
          <div>{data?.copyright}</div>
        </div>
      </div>
    </footer>
  );
}
