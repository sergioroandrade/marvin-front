import Image from 'next/image';
import gallery from './gallery.module.scss';

export function Gallery({ images }) {
  if (!images?.length) return false;

  function renderImages(imgs) {
    return imgs?.map((image, index) => {
      return (
        <div className={gallery.item} key={index}>
          <Image
            src={image?.url}
            width={600}
            height={600}
            objectFit='cover'
            alt={image?.alternativeText}
          />
        </div>
      );
    });
  }

  return (
    <div className={gallery.wrapper}>
      <div className={gallery.row}>{images && renderImages(images)}</div>
    </div>
  );
}
