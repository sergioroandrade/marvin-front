import { Logo, Menu, SideNav } from 'components';
import { HamburgerIcon } from 'icons';
import { useEffect, useState } from 'react';
import header from './header.module.scss';

export function Header() {
  const menus = [
    {
      href: '/sobre',
      text: 'Sobre',
    },
    {
      href: '/#destaques',
      text: 'Destaques',
    },
    {
      href: '/projetos',
      text: 'Produtores',
    },
    {
      href: '/origem',
      text: 'Origem',
    },
    {
      href: './#contato',
      text: 'Contato',
    },
  ];

  const [isOpenedMenu, setIsOpenedMenu] = useState(false);
  const [headerTransparent, setHeaderTransparent] = useState(false);
  const [shouldBeTransparent, setShouldBeTransparent] = useState(false);

  useEffect(() => {
    setShouldBeTransparent(true);
    window.addEventListener('scroll', handleWindowScroll);

    window.dispatchEvent(new Event('scroll'));
  });

  function handleClickOpen() {
    setIsOpenedMenu(true);
  }

  function handleClickClose() {
    setIsOpenedMenu(false);
  }

  function handleWindowScroll() {
    const OFFSET_TO_CHANGE_MENU = 300;
    const isWindowScrollyLtOffset = window.scrollY <= OFFSET_TO_CHANGE_MENU;
    setHeaderTransparent(isWindowScrollyLtOffset);
  }

  const isTransparent = shouldBeTransparent && headerTransparent;

  return (
    <header
      className={`
      ${header.header}
      ${isTransparent ? header.transparent : header.white}
    `}
    >
      <section className={header.container}>
        <Logo variant={isTransparent ? 'light' : 'dark'} />
        <Menu menus={menus} />
        <SideNav
          isOpenedMenuProp={isOpenedMenu}
          handleClickClose={handleClickClose}
          menus={menus}
        />
        <button className={header.hamburger} onClick={() => handleClickOpen()}>
          <HamburgerIcon
            className={isTransparent ? header.light : header.dark}
          />
        </button>
      </section>
    </header>
  );
}
