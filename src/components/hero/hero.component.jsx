import { Title } from 'components';
import hero from './hero.module.scss';

export function Hero({ title, image }) {
  const img = image && { backgroundImage: `url(${image})` };
  return (
    <section className={hero.banner} style={img}>
      <div className={hero.content}>
        <Title hasRibbon={true}>{title}</Title>
      </div>
    </section>
  );
}
