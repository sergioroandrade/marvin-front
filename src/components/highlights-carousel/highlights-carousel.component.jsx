import { Thumbnail } from 'components';
import { useRef } from 'react';
import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import carousel from './highlights-carousel.module.scss';
export function HighlightsCarousel({ wines }) {
  if (!wines) return null;

  const breakpoints = {
    200: {
      slidesPerView: 1.3,
    },
    992: {
      slidesPerView: 3,
      pagination: false,
      spaceBetween: 20,
    },
  };

  SwiperCore.use([Navigation]);
  const prevRef = useRef(null);
  const nextRef = useRef(null);
  return (
    <div className={carousel.wrapper}>
      <div className={carousel.content}>
        <div ref={prevRef} className='swiper-button-prev'></div>
        <div ref={nextRef} className='swiper-button-next'></div>
        <div className={carousel.container}>
          <Swiper
            loop={true}
            spaceBetween={0}
            slidesPerView={3}
            preloadImages={false}
            breakpoints={breakpoints}
            onInit={(swiper) => {
              swiper.params.navigation.prevEl = prevRef.current;
              swiper.params.navigation.nextEl = nextRef.current;
              swiper.navigation.init();
              swiper.navigation.update();
            }}
          >
            {wines &&
              wines
                ?.filter((wine) => wine.highlight)
                ?.map((wine, index) => {
                  return (
                    <SwiperSlide key={index}>
                      <div className={carousel.item}>
                        <Thumbnail
                          image={wine?.bottle?.url}
                          name={wine?.name}
                          project={wine?.project?.name}
                          link={`${wine?.project?.slug}#${wine?.id}`}
                          country={wine?.country}
                        />
                      </div>
                    </SwiperSlide>
                  );
                })}
          </Swiper>
        </div>
      </div>
    </div>
  );
}
