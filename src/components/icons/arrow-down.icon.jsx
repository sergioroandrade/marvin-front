export function ArrowDownIcon(props) {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 21.991 10.988'
      {...props}
    >
      <path
        d='M367.816,1270.5l7.012,9.87L367,1289.342'
        transform='matrix(0.035, 0.999, -0.999, 0.035, 1277.208, -409.657)'
        fill='none'
        stroke='#000'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
      />
    </svg>
  );
}
