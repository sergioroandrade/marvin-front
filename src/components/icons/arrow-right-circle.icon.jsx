export function ArrowRightCircleIcon(props) {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 28 28' {...props}>
      <g fill='none'>
        <path
          d='M11.938 8.5l5.262 5.305-5.7 4.823'
          stroke='#000'
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeWidth='2'
        />
        <path d='M14 0A14 14 0 110 14 14 14 0 0114 0z' />
        <path
          d='M14 1c-3.4724 0-6.737 1.3522-9.1924 3.8076C2.3522 7.263 1 10.5276 1 14c0 3.4724 1.3522 6.737 3.8076 9.1924C7.263 25.6478 10.5276 27 14 27c3.4724 0 6.737-1.3522 9.1924-3.8076C25.6478 20.737 27 17.4724 27 14c0-3.4724-1.3522-6.737-3.8076-9.1924C20.737 2.3522 17.4724 1 14 1m0-1c7.732 0 14 6.268 14 14s-6.268 14-14 14S0 21.732 0 14 6.268 0 14 0z'
          fill='#000'
        />
      </g>
    </svg>
  );
}
