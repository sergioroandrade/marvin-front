export function ArrowRightIcon(props) {
  return (
    <svg
      width='52'
      height='9'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M49.545 3.775H0v1.259h49.635L46.66 8.008l.89.89 3.56-3.559.89-.89-.89-.89L47.55 0l-.89.89 2.885 2.885z'
        fill='#fff'
      />
    </svg>
  );
}
