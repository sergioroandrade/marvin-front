export function CloseIcon(props) {
  return (
    <svg
      viewBox='0 0 25 25'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M3.182 1.06a1.5 1.5 0 00-2.121 2.122l9.382 9.382-9.382 9.382a1.5 1.5 0 002.121 2.122l9.382-9.383 9.382 9.383a1.5 1.5 0 102.122-2.122l-9.383-9.382 9.383-9.382a1.5 1.5 0 10-2.122-2.121l-9.382 9.382L3.182 1.06z'
        fill='#fff'
      />
    </svg>
  );
}
