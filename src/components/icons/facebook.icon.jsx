export function FacebookIcon(props) {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 27 27' {...props}>
      <path
        d='M15.211 9.325V5.986a1.671 1.671 0 011.67-1.67h1.67V.141h-3.34a5.009 5.009 0 00-5.01 5.01v4.174H6.862V13.5h3.34v13.359h5.01V13.5h3.34l1.67-4.175z'
        fill='#fff'
      />
    </svg>
  );
}
