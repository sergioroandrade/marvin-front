export function HamburgerIcon(props) {
  return (
    <svg
      viewBox='0 0 41 22'
      width='41'
      height='22'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M39.5 0h-38C.6714 0 0 .6716 0 1.5S.6714 3 1.5 3h38c.8286 0 1.5-.6716 1.5-1.5S40.3286 0 39.5 0zm0 9h-38C.6714 9 0 9.6716 0 10.5S.6714 12 1.5 12h38c.8286 0 1.5-.6716 1.5-1.5S40.3286 9 39.5 9zm-38 10h38c.8286 0 1.5.6716 1.5 1.5s-.6714 1.5-1.5 1.5h-38C.6714 22 0 21.3284 0 20.5S.6714 19 1.5 19z'
        fill='#fff'
      />
    </svg>
  );
}
