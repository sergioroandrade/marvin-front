export function PlayIcon(props) {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='110'
      height='110'
      viewBox='0 0 110 110'
    >
      <g transform='translate(-10271 -8730)'>
        <circle
          data-name='Elipse 55'
          cx='55'
          cy='55'
          r='55'
          transform='translate(10271 8730)'
          fill='#fff'
          opacity='.63'
        />
        <path
          data-name='Caminho 106'
          d='M10314.236 8754.961v60.376l33.086-28.766z'
        />
      </g>
    </svg>
  );
}
