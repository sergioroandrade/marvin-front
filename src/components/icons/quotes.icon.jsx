export function Quotes(props) {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='67'
      height='54'
      viewBox='0 0 67 54'
      {...props}
    >
      <path
        d='M-30-124.5V-135h6q8,0,8-8v-8h-3.25a14.233,14.233,0,0,1-10.125-3.875A12.956,12.956,0,0,1-33.5-164.75a12.956,12.956,0,0,1,4.125-9.875A14.233,14.233,0,0,1-19.25-178.5a13.766,13.766,0,0,1,10.125,4.125A13.766,13.766,0,0,1-5-164.25v20.75q0,19-18.75,19Zm38.5,0V-135h6q8,0,8-8v-8H19.25a14.233,14.233,0,0,1-10.125-3.875A12.956,12.956,0,0,1,5-164.75a12.956,12.956,0,0,1,4.125-9.875A14.233,14.233,0,0,1,19.25-178.5a13.766,13.766,0,0,1,10.125,4.125A13.766,13.766,0,0,1,33.5-164.25v20.75q0,19-18.75,19Z'
        transform='translate(33.5 178.5)'
        fill='#ccc'
      />
    </svg>
  );
}
