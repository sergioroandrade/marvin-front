export function RibbonIcon(props) {
  return (
    <svg
      width='203'
      height='10'
      viewBox='0 0 203 10'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M94.162 0l-4 4H3V3H0v3h3V5h87l4.162 4.162 4.581-4.581L94.163 0zm14.674 0l4 4h87.163V3h3v3h-3V5h-87.001l-4.162 4.162-4.581-4.581L108.836 0z'
        fill='#DCDCDC'
      />
    </svg>
  );
}
