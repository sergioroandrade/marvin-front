import input from './input.module.scss';

export function Input({ type = 'text', label, value, ...props }) {
  function renderInput() {
    return (
      <input className={input.field} type={type} value={value} {...props} />
    );
  }

  function renderTextarea() {
    return (
      <textarea className={input.field} type={type} value={value} {...props} />
    );
  }

  return (
    <label className={input.wrapper}>
      <span className={input.label}>{label}</span>
      {type === 'textarea' ? renderTextarea() : renderInput()}
    </label>
  );
}
