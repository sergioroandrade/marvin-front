import { Title } from 'components';
import intro from './intro.module.scss';
export function Intro({ title, text }) {
  return (
    <section className={intro.wrapper}>
      <div className={intro.container}>
        <div className={intro.row}>
          <div className={intro.column}>
            <Title variant='wine'>{title}</Title>
            <div>{text}</div>
          </div>
          <div className={intro.column}>
            <div className={intro.image} />
          </div>
        </div>
      </div>
    </section>
  );
}
