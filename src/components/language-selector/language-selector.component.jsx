import { useLanguage } from 'providers';
import React from 'react';
import lang from './language-selector.module.scss';

export function LanguageSelector({ className = '' }) {
  const { language, setLanguage } = useLanguage();

  function handleClickChangeLanguage(lang) {
    setLanguage(lang);
    localStorage.setItem('language', lang);
  }

  return (
    <div className={`${lang.wrapper} ${className}`}>
      <button
        className={`${lang.button} ${language === 'pt-BR' ? lang.active : ''}`}
        onClick={() => handleClickChangeLanguage('pt-BR')}
      >
        Português
      </button>
      <button
        className={`${lang.button} ${language === 'en' ? lang.active : ''}`}
        onClick={() => handleClickChangeLanguage('en')}
      >
        English
      </button>
    </div>
  );
}
