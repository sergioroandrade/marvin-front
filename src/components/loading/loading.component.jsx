import { useLoading } from 'providers';
import loading from './loading.module.scss';

export function Loading() {
  const { loaderVisible } = useLoading();
  if (!loaderVisible) return false;
  return <div className={loading.wrapper} />;
}
