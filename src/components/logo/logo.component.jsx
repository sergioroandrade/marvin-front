import { LogoIcon } from 'icons';
import logo from './logo.module.scss';

export function Logo({ variant, className = '' }) {
  const variations = {
    dark: logo.dark,
    light: logo.light,
  };

  const currentVariant = variations[variant] || logo.light;

  return (
    <a href='/' className={`${className} ${logo.wrapper}`}>
      <LogoIcon className={currentVariant} />
    </a>
  );
}
