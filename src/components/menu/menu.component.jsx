import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import menu from './menu.module.scss';

export function Menu({ menus }) {
  const [pathName, setPathName] = useState();
  const [isOpenSubMenu, setIsOpenSubMenu] = useState();
  const router = useRouter();

  const menuItemRef = React.createRef();
  const menuListRef = React.createRef();

  useEffect(() => {
    setPathName(router.pathname);
  });

  function isActiveMenu(href) {
    return pathName.includes(href);
  }

  function setPostionToSubmenuBasedOnLiRef() {
    const padding = 20;
    const left = menuItemRef.current.offsetLeft - padding;
    menuListRef.current.style.marginLeft = `${left}px`;
  }

  function handleMouseEnterSubmenu() {
    setPostionToSubmenuBasedOnLiRef();
    setIsOpenSubMenu(true);
  }

  function handleMouseLeaveSubmenu() {
    setIsOpenSubMenu(false);
  }

  function handleClickSubMenu() {
    setIsOpenSubMenu(false);
  }

  function renderSubmenu(submenus) {
    return submenus.map((item, index) => {
      return (
        <li
          key={index}
          className={menu.item}
          onClick={() => handleClickSubMenu()}
        >
          <Link href={item.href}>{item.text}</Link>
        </li>
      );
    });
  }

  function renderMenuList(menus) {
    return menus.map((item, index) => {
      if (!pathName) return null;

      if (item.submenu) {
        return (
          <li
            key={index}
            className={menu.item}
            onMouseEnter={handleMouseEnterSubmenu}
            onMouseLeave={handleMouseLeaveSubmenu}
            ref={menuItemRef}
            className={`${menu.item} ${
              isActiveMenu(item.href) ? menu.active : ''
            }`}
          >
            <Link href={item.href}>{item.text}</Link>
            {item.submenu && (
              <div
                className={`${menu.submenu} ${
                  isOpenSubMenu ? menu.opened : ''
                }`}
              >
                <ul className={menu.list} ref={menuListRef}>
                  {renderSubmenu(item.submenu)}
                </ul>
              </div>
            )}
          </li>
        );
      }

      return (
        <li
          key={index}
          className={`${menu.item} ${
            isActiveMenu(item.href) ? menu.active : ''
          }`}
        >
          <Link className={menu.link} href={item.href}>
            {item.text}
          </Link>
        </li>
      );
    });
  }
  return (
    <div className={menu.menu}>
      <ul className={menu.list}>{renderMenuList(menus)}</ul>
    </div>
  );
}
