import { Button, Title } from 'components';
import Image from 'next/image';
import ReactHtmlParser from 'react-html-parser';
import item from './project-item.module.scss';

export function ProjectItem({ title, text, image, link }) {
  return (
    <div className={item.wrapper}>
      <div className={item.image}>
        <Title className={item.title} tag='h2'>
          {title}
        </Title>
        <Image
          src={image}
          height={400}
          width={600}
          objectFit='cover'
          alt={title}
        />
      </div>
      <div className={item.text}>{ReactHtmlParser(text)}</div>
      {link && (
        <Button className={item.button} href={link.href}>
          {link.text}
        </Button>
      )}
    </div>
  );
}
