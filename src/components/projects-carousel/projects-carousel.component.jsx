import { ContentBlock, ProjectItem } from 'components';
import { useRef } from 'react';
import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import carousel from './projects-carousel.module.scss';

export function ProjectsCarousel({ projects, title }) {
  SwiperCore.use([Navigation]);
  const prevRef = useRef(null);
  const nextRef = useRef(null);

  const breakpoints = {
    200: {
      slidesPerView: 1.2,
    },
    992: {
      slidesPerView: 1,
      pagination: false,
      spaceBetween: 20,
    },
  };

  return (
    <div className={carousel.wrapper}>
      {title && <h3 className={carousel.title}>{title}</h3>}
      <div className={carousel.content}>
        <div ref={prevRef} className='swiper-button-prev'></div>
        <div ref={nextRef} className='swiper-button-next'></div>
        <Swiper
          loop={true}
          preloadImages={false}
          breakpoints={breakpoints}
          onInit={(swiper) => {
            swiper.params.navigation.prevEl = prevRef.current;
            swiper.params.navigation.nextEl = nextRef.current;
            swiper.navigation.init();
            swiper.navigation.update();
          }}
        >
          {projects &&
            projects.map((project, index) => {
              return (
                <SwiperSlide key={index}>
                  <div className={carousel.item}>
                    <div className={carousel.mobile}>
                      <ProjectItem
                        headline={project?.headline}
                        title={project?.name}
                        text={project?.short_description}
                        image={project?.images[0].url}
                        link={{
                          href: `/projetos/${project?.slug}`,
                          text: 'Saiba Mais',
                        }}
                      />
                    </div>
                    <div className={carousel.desktop}>
                      <ContentBlock
                        className={carousel.block}
                        headline={project?.headline}
                        title={project?.name}
                        text={project?.short_description}
                        image={project?.images[0].url}
                        inverse={true}
                        link={{
                          href: `/projetos/${project?.slug}`,
                          text: 'Saiba Mais',
                        }}
                      />
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
        </Swiper>
      </div>
    </div>
  );
}
