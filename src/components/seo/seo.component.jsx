import Head from 'next/head';

export function Seo({ data }) {
  if (!data) return false;

  return (
    <>
      <Head>
        <>
          <meta charSet='utf-8' />
          <meta name='language' content='pt-BR' />
          <title>{data?.title}</title>
          <meta name='description' content={data?.description} />
          <meta name='author' content='Marvin Importadora' />
          <meta name='keywords' content={data?.keywords} />

          <meta property='og:type' content='page' />
          <meta property='og:url' content={data?.url} />
          <meta property='og:title' content={data?.title} />
          <meta property='og:image' content={data?.image?.url} />
          <meta property='og:description' content={data?.description} />
          <meta property='article:author' content='Marvin Importadora' />
          <link rel='canonical' href={data?.url} />
        </>
      </Head>
    </>
  );
}
