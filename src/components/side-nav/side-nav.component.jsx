import { Logo } from 'components';
import { ArrowDownIcon, CloseIcon } from 'icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import sideNav from './side-nav.module.scss';

export function SideNav({ isOpenedMenuProp, handleClickClose, menus }) {
  const [isOpenedSubmenu, setIsOpenedSubmenu] = useState(false);
  const router = useRouter();
  const [pathName, setPathName] = useState();

  useEffect(() => {
    setPathName(router.pathname);
  });

  function handleClickOpenSubmenu() {
    setIsOpenedSubmenu(!isOpenedSubmenu);
  }

  function renderSubmenu(submenus) {
    return submenus.map((item, index) => {
      return (
        <li
          key={index}
          className={sideNav.item}
          onClick={() => handleClickClose()}
        >
          <Link href={item.href}>{item.text}</Link>
        </li>
      );
    });
  }

  function isActiveMenu(href) {
    return pathName.includes(href);
  }

  function renderMenuList(menus) {
    if (!pathName) return null;
    return menus.map((item, index) => {
      if (item.submenu) {
        return (
          <li key={index} className={sideNav.item}>
            <button
              onClick={handleClickOpenSubmenu}
              className={`${sideNav.button} ${
                isOpenedSubmenu ? sideNav.opened : ''
              }`}
            >
              {item.text}
              <ArrowDownIcon className={sideNav.arrow} />
            </button>
            {item.submenu && (
              <div
                className={`${sideNav.submenu} ${
                  isOpenedSubmenu ? sideNav.opened : ''
                }`}
              >
                <ul className={sideNav.list}>{renderSubmenu(item.submenu)}</ul>
              </div>
            )}
          </li>
        );
      }
      return (
        <li
          key={index}
          onClick={() => handleClickClose()}
          className={`${sideNav.item} ${
            isActiveMenu(item.href) ? sideNav.active : ''
          }`}
        >
          <Link href={item.href}>{item.text}</Link>
        </li>
      );
    });
  }

  return (
    <>
      <button
        className={`${sideNav.overlay} ${
          isOpenedMenuProp ? sideNav.opened : ''
        }`}
        onClick={() => handleClickClose()}
      ></button>
      <div
        className={`${sideNav.wrapper} ${
          isOpenedMenuProp ? sideNav.opened : ''
        }`}
      >
        <div className={sideNav.content}>
          <div className={sideNav.inner}>
            <button
              className={sideNav.close}
              onClick={() => handleClickClose()}
            >
              <CloseIcon />
            </button>
            <Logo className={sideNav.logo} />
            <ul className={sideNav.list}>{renderMenuList(menus)}</ul>
          </div>
        </div>
      </div>
    </>
  );
}
