import small from './small.module.scss';

export function Small({ children, className = '' }) {
  return <small className={`${small.wrapper} ${className}`}>{children}</small>;
}
