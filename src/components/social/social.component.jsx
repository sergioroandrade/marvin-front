import { FacebookIcon, InstagramIcon } from 'icons';
import social from './social.module.scss';

export function Social({ facebook, instagram, className }) {
  function renderFacebook(link) {
    return (
      <li className={social.item}>
        <a
          href={link}
          target='blank'
          rel='noopener noreferrer'
          className={social.link}
        >
          <FacebookIcon />
        </a>
      </li>
    );
  }

  function renderInstagram(link) {
    return (
      <li className={social.item}>
        <a
          href={link}
          target='blank'
          rel='noopener noreferrer'
          className={social.link}
        >
          <InstagramIcon />
        </a>
      </li>
    );
  }
  return (
    <ul className={`${social.wrapper} ${className}`}>
      {instagram && renderInstagram(instagram)}
      {facebook && renderFacebook(facebook)}
    </ul>
  );
}
