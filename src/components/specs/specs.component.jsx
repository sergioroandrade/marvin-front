import { Flag, Wine } from 'components';
import { GrapeIcon, MapIcon } from 'icons';
import specs from './specs.module.scss';

export function Specs({ wine }) {
  if (!wine) return null;
  return (
    <>
      <table className={specs.table}>
        <thead></thead>
        <tbody>
          <tr>
            <td>
              <div className={specs.cell}>
                <Wine className={specs.icon} variant={wine.product} />
                <span className={specs.label}>{wine.product}</span>
              </div>
            </td>
            <td>
              <div className={specs.cell}>
                <Flag className={specs.icon} country={wine.country} />
                <span className={specs.label}>{wine.country}</span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div className={specs.cell}>
                <MapIcon className={specs.icon} />
                <span className={specs.label}>{wine.region}</span>
              </div>
            </td>
            <td>
              <div className={specs.cell}>
                <GrapeIcon className={specs.icon} />
                <span className={specs.label}>{wine.grape}</span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      {wine?.variety && (
        <div>
          <strong>Variedade: </strong>
          {wine?.variety}
        </div>
      )}
      {wine?.alcohol && (
        <div>
          <strong>Álcool: </strong>
          {wine?.alcohol}
        </div>
      )}
      {wine?.harvest && (
        <div>
          <strong>Safra: </strong>
          {wine?.harvest}
        </div>
      )}
      {wine?.regions && (
        <div>
          <strong>Vinhedo: </strong>
          {wine?.regions}
        </div>
      )}
    </>
  );
}
