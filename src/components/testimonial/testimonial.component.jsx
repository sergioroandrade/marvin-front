import ReactHtmlParser from 'react-html-parser';
import testimonial from './testimonial.module.scss';

export function Testimonial({ text, author }) {
  return (
    <div className={testimonial.wrapper}>
      <div className={testimonial.text}>
        <div>{ReactHtmlParser(text)}</div>
      </div>
      <p className={testimonial.author}>{author}</p>
    </div>
  );
}
