import { Flag } from 'components';
import Image from 'next/image';
import Link from 'next/link';
import thumb from './thumbnail.module.scss';

export function Thumbnail({ image, name, project, link, country }) {
  return (
    <Link href={`/projetos/${link}`}>
      <div className={thumb.wrapper}>
        <div className={thumb.border}>
          <Flag className={thumb.flag} country={country} />
          <div className={thumb.image}>
            {image && (
              <Image
                src={image}
                width={200}
                height={300}
                objectPosition='center'
                objectFit='contain'
                alt={`${name} | ${project}`}
              />
            )}
          </div>
          <strong className={thumb.name}>{name}</strong>
          <span className={thumb.project}>{project}</span>
        </div>
      </div>
    </Link>
  );
}
