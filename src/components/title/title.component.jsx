import { RibbonIcon } from 'icons';
import title from './title.module.scss';

export function Title({
  variant,
  tag = 'h1',
  hasRibbon = false,
  children,
  className = '',
}) {
  const variations = {
    wine: title.wine,
  };
  const currentVariant = variations[variant] || title.light;
  const Tag = tag;
  return (
    <Tag className={`${title.wrapper} ${currentVariant} ${className}`}>
      <span>{children}</span>
      {hasRibbon && <RibbonIcon className={title.icon} />}
    </Tag>
  );
}
