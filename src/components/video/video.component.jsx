import { PlayIcon } from 'icons';
import Image from 'next/image';
import { useState } from 'react';
import video from './video.module.scss';

export function Video({ url, cover, className }) {
  const [canShowVideo, setCanShowVideo] = useState(false);

  if (!url) return false;

  function getYoutubeVideoId(url) {
    let id = '';
    url = url
      .replace(/(>|<)/gi, '')
      .split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if (url[2] !== undefined) {
      id = url[2].split(/[^0-9a-z_\-]/i);
      id = id[0];
    } else {
      id = url;
    }
    return id;
  }

  const videoId = getYoutubeVideoId(url);

  function handleClickPlay() {
    setCanShowVideo(true);
  }

  function renderIframe() {
    return (
      <iframe
        className={video.iframe}
        width='1920'
        height='1080'
        src={`https://www.youtube.com/embed/${videoId}?showinfo=0&rel=0&playlist=${videoId}&fs=0&loop=1&autoplay=1`}
        frameBorder='0'
        loop={true}
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        allowFullScreen
      ></iframe>
    );
  }

  return (
    <div className={`${video.wrapper} ${className}`}>
      <div className={video.inside}>
        {!canShowVideo && (
          <button className={video.play} onClick={handleClickPlay}>
            <PlayIcon />
          </button>
        )}

        {cover && (
          <Image
            src={cover}
            width='1920'
            height='1080'
            layout='responsive'
            alt=''
          />
        )}

        {canShowVideo && renderIframe()}
      </div>
    </div>
  );
}
