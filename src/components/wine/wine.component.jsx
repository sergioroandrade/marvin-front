import { WineIcon } from 'icons';
import wine from './wine.module.scss';

export function Wine({ variant, className = '' }) {
  const variations = {
    branco: wine.white,
    tinto: wine.tint,
    rose: wine.rose,
  };

  const currentVariant = variant
    ? variations[variant?.toLowerCase()]
    : wine.tint;

  return (
    <div className={`${className} ${wine.wrapper}`}>
      <WineIcon className={currentVariant} />
    </div>
  );
}
