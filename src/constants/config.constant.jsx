export const API_URL = process.env.NEXT_PUBLIC_API_URL;
export const IMAGES_URL = process.env.NEXT_PUBLIC_IMAGES_DOMAIN;
export const REVALIDATE_TIME = process.env.NEXT_PUBLIC_REVALIDATE_TIME;
