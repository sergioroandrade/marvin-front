import axios from 'axios';
import { API_URL } from 'constants';
import { useLoading } from 'providers';
const instance = axios.create({
  baseURL: API_URL,
});

export const useApi = (path) => {
  const { setLoaderVisible } = useLoading(false);

  const callApi = async (config) => {
    setLoaderVisible(true);
    config.url = buildUrl(config.url);
    config.data = buildData(config.data);

    try {
      const result = await instance.request(config);
      return result.data;
    } finally {
      setLoaderVisible(false);
    }
  };

  const buildData = (data) => data || null;
  const buildUrl = (url) => (url ? `${path}/${url}` : path);

  return {
    get: (url, config = {}) => {
      return callApi({ method: 'GET', url, ...config });
    },
    post: (url, data, config = {}) => {
      return callApi({ method: 'POST', url, data, ...config });
    },
    put: (url, data, config = {}) => {
      return callApi({ method: 'PUT', url, data, ...config });
    },
    del: (url, config = {}) => {
      return callApi({ method: 'DELETE', url, ...config });
    },
  };
};
