import {
  Analytics,
  Banner,
  Contact,
  ContentBlock,
  Footer,
  HighlightsCarousel,
  ProjectsCarousel,
  Seo,
  Small,
  Title,
} from 'components';
import home from './index.module.scss';

export default function Home({
  about,
  origin,
  projects,
  wines,
  footer,
  phone,
  banner,
  seo,
  analitics,
}) {
  return (
    <>
      <Analytics code={analitics.cod} />
      <Seo data={seo} />
      <Banner data={banner} />

      <section className={home.about} id='sobre'>
        <ContentBlock
          headline={about?.headline}
          title={about?.title}
          text={about?.description}
          image={about?.image?.url}
          inverse={true}
          link={{
            href: '/sobre/',
            text: 'Saiba Mais',
          }}
        />
      </section>

      <section className={home.highlights} id='destaques'>
        <div className={home.highlights__title}>
          <Small>NOSSOS VINHOS</Small>
          <Title hasRibbon={true}>Destaques</Title>
        </div>
        <div className={home.highlights__container}>
          <HighlightsCarousel wines={wines} />
        </div>
      </section>

      <section className={home.projects}>
        <div className={home.projects__title}>
          <Title variant='wine' hasRibbon={true} tag='h2'>
            Produtores
          </Title>
        </div>
        <div className={home.projects__container}>
          <ProjectsCarousel projects={projects} />
        </div>
      </section>

      <section className={home.origin} id='origem'>
        <ContentBlock
          headline={origin?.headline}
          title={origin?.title}
          text={origin?.description}
          image={origin?.image?.url}
          inverse={true}
        />
      </section>

      <Contact phone={phone} />

      <Footer data={footer} />
    </>
  );
}

export async function getStaticProps() {
  const about = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/about`);
  const aboutData = await about.json();

  const origin = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/country`);
  const originData = await origin.json();

  const projects = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/projects?_where&Active=true`
  );
  const projectsData = await projects.json();

  const wines = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/wines`);
  const winesData = await wines.json();

  const footer = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/footer`);
  const footerData = await footer.json();

  const contact = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/contact`);
  const contactData = await contact.json();

  const banner = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/banner`);
  const bannerData = await banner.json();

  const seo = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/seo`);
  const seoData = await seo.json();

  const analitics = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/ga`);
  const analiticsData = await analitics.json();

  return {
    props: {
      about: aboutData ?? {},
      origin: originData ?? {},
      projects: projectsData ?? {},
      wines: winesData ?? {},
      footer: footerData ?? {},
      phone: contactData?.phone ?? {},
      banner: bannerData ?? {},
      seo: seoData ?? {},
      analitics: analiticsData ?? {},
    },
    revalidate: 23400,
  };
}
