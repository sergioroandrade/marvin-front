import {
  Analytics,
  Contact,
  ContentBlock,
  Footer,
  Gallery,
  Hero,
  Seo,
} from 'components';
import style from './origin.module.scss';

export default function Origem({ origin, footer, phone, seo, analitics }) {
  const seoData = seo;
  seoData.title = `Marvin | Nossas Origens`;
  return (
    <>
      <Analytics code={analitics.cod} />
      <Seo data={seoData} />
      <Hero title='Nossas Origens' image={origin?.banner?.url} />
      {origin && (
        <ContentBlock
          headline={origin?.headline}
          title={origin?.title}
          text={origin?.description}
          image={origin?.image?.url}
          inverse={true}
        />
      )}

      <div className={style.container}>
        <Gallery images={origin?.gallery} />
      </div>
      <Contact phone={phone} />
      <Footer data={footer} />
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/country`);
  const response = await res.json();

  const footer = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/footer`);
  const footerData = await footer.json();

  const contact = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/contact`);
  const contactData = await contact.json();

  const seo = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/seo`);
  const seoData = await seo.json();

  const analitics = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/ga`);
  const analiticsData = await analitics.json();

  return {
    props: {
      origin: response ?? {},
      footer: footerData ?? {},
      phone: contactData?.phone ?? {},
      seo: seoData ?? {},
      analitics: analiticsData ?? {},
    },
    revalidate: 23400,
  };
}
