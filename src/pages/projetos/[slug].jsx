import {
  Analytics,
  Contact,
  ContentBlock,
  Divisor,
  Footer,
  Hero,
  Seo,
  Small,
  Title,
} from 'components';
import project from './project.module.scss';

export default function Projeto({
  data,
  footer,
  phone,
  seo,
  wines,
  analitics,
}) {
  const seoData = seo;
  seoData.title = `Marvin | ${data?.name}`;
  function renderWines(wines) {
    if (!wines) return null;
    return (
      <section className={project.wines}>
        <div className={project.wines__title}>
          <Small>Vinhos de </Small>
          <Title hasRibbon={true} variant={'wine'} tag='h3'>
            {data?.name}
          </Title>
        </div>
        {wines?.map((wine, index) => {
          return (
            <div key={index} id={wine?.id}>
              <ContentBlock
                headline={wine?.type}
                title={wine?.name}
                image={wine?.image?.url}
                imageWidth={wine?.image?.width}
                imageHeight={wine?.image?.height}
                text={wine?.description}
                inverse={index % 2}
                wine={wine}
              />
              {index < wines?.wines?.length - 1 && <Divisor />}
            </div>
          );
        })}
      </section>
    );
  }
  return (
    <>
      <Analytics code={analitics.cod} />
      <Seo data={seoData} />
      <Hero title={data?.name} image={data?.images[0].url} />
      <ContentBlock
        title={data?.name}
        text={data?.description}
        image={data?.images[0].url}
        imageWidth={data?.images[0]?.width}
        imageHeight={data?.images[0]?.height}
      />
      {renderWines(wines)}
      <Contact phone={phone} />
      <Footer data={footer} />
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/projects?slug=${context.params.slug}`
  );
  const response = await res.json();

  const footer = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/footer`);
  const footerData = await footer.json();

  const contact = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/contact`);
  const contactData = await contact.json();

  const seo = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/seo`);
  const seoData = await seo.json();

  const wine = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/wines?project.slug=${context.params.slug}&_sort=order:ASC`
  );
  const wineData = await wine.json();

  const analitics = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/ga`);
  const analiticsData = await analitics.json();

  return {
    props: {
      data: response[0] ?? {},
      wines: wineData ?? {},
      footer: footerData ?? {},
      phone: contactData?.phone ?? {},
      seo: seoData ?? {},
      analitics: analiticsData ?? {},
    },
    revalidate: 23400,
  };
}

export async function getStaticPaths() {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/projects`);
  const response = await res.json();

  const paths = response.map((projects) => `/projetos/${projects.slug}`);

  return {
    paths,
    fallback: 'blocking',
  };
}
