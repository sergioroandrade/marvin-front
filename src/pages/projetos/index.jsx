import {
  Analytics,
  Contact,
  ContentBlock,
  Divisor,
  Footer,
  Hero,
  Seo,
} from 'components';

export default function Projetos({ data, footer, phone, seo, analitics }) {
  const seoData = seo;
  seoData.title = `Marvin | Produtores`;
  function renderContent(data) {
    if (!data) return null;
    return data.map((project, index) => {
      return (
        <div key={index}>
          <ContentBlock
            title={project?.name}
            text={project?.short_description}
            inverse={true}
            image={project?.images[0].url}
            imageWidth={project?.images[0].width}
            imageHeight={project?.images[0].height}
            link={{
              href: `/projetos/${project?.slug}`,
              text: 'Saiba Mais',
            }}
          />
          {index < data.length - 1 && <Divisor />}
        </div>
      );
    });
  }

  return (
    <>
      <Analytics code={analitics?.cod} />
      <Seo data={seoData} />
      <Hero title='Produtores' />
      {data && renderContent(data)}
      <Contact phone={phone} />
      <Footer data={footer} />
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/projects?_sort=order:ASC&_where&Active=true`
  );
  const response = await res.json();

  const footer = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/footer`);
  const footerData = await footer.json();

  const contact = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/contact`);
  const contactData = await contact.json();

  const seo = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/seo`);
  const seoData = await seo.json();

  const analitics = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/ga`);
  const analiticsData = await analitics.json();

  return {
    props: {
      data: response ?? {},
      footer: footerData ?? {},
      phone: contactData?.phone ?? {},
      seo: seoData ?? {},
      analitics: analiticsData ?? {},
    },
    revalidate: 23400,
  };
}
