import { Button, Intro } from 'components';
import { useState } from 'react';

export default function Publicar() {
  const [isPublishing, setIsPublishing] = useState(false);

  function handleSubmit(event) {
    event.preventDefault();
    sendForm();
  }

  async function sendForm() {
    try {
      await fetch(
        'https://api.vercel.com/v1/integrations/deploy/prj_b5WYPOMNGBWzGT63ULjp8zA1P1YR/FSnNQtd05E',
        {
          method: 'POST',
        }
      );
      setIsPublishing(true);
    } catch (e) {
      console.log(e, 'ERRO');
    }
  }

  const style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    background: '#fff',
  };

  return (
    <div style={style}>
      {!isPublishing && <Button onClick={handleSubmit}>Publicar</Button>}
      {isPublishing && (
        <>
          <Intro
            title='Publicando as alterações'
            text='Aguarde pelo menos 5 minutos para ver as alterações refletirem no site.'
          ></Intro>
        </>
      )}
    </div>
  );
}
