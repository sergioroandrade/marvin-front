import {
  Analytics,
  Contact,
  ContentBlock,
  Footer,
  Gallery,
  Hero,
  Seo,
  Testimonial,
  Title,
} from 'components';
import { useRef } from 'react';
import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import style from './about.module.scss';
export default function Sobre({
  about,
  footer,
  phone,
  seo,
  testimonials,
  analitics,
}) {
  SwiperCore.use([Navigation]);
  const prevRef = useRef(null);
  const nextRef = useRef(null);

  const seoData = seo;
  seoData.title = `Marvin | Sobre nós`;

  return (
    <>
      <Analytics code={analitics?.cod} />
      <Seo data={seoData} />
      <Hero title='Sobre nós' image={about?.banner?.url} />
      {about && (
        <ContentBlock
          headline={about?.headline}
          title={about?.title}
          text={about?.description}
          image={about?.image?.url}
          inverse={true}
        />
      )}
      <center>
        <Title hasRibbon={true} variant={'wine'} tag='h3'>
          Depoimentos
        </Title>
      </center>
      <div className={style.container} id='testimonials-slider'>
        <div ref={prevRef} className='swiper-button-prev'></div>
        <div ref={nextRef} className='swiper-button-next'></div>
        <Swiper
          loop={true}
          preloadImages={false}
          onInit={(swiper) => {
            swiper.params.navigation.prevEl = prevRef.current;
            swiper.params.navigation.nextEl = nextRef.current;
            swiper.navigation.init();
            swiper.navigation.update();
          }}
        >
          {testimonials &&
            testimonials.map((testimonial, index) => {
              return (
                <SwiperSlide key={index}>
                  <Testimonial
                    text={testimonial?.testimonial}
                    author={testimonial?.author}
                  />
                </SwiperSlide>
              );
            })}
        </Swiper>
      </div>
      <div className={style.container}>
        <Gallery images={about?.gallery} />
      </div>
      <Contact phone={phone} />
      <Footer data={footer} />
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/about`);
  const response = await res.json();

  const footer = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/footer`);
  const footerData = await footer.json();

  const contact = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/contact`);
  const contactData = await contact.json();

  const seo = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/seo`);
  const seoData = await seo.json();

  const testimonials = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/testimonials`
  );
  const testimonialsData = await testimonials.json();

  const analitics = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/ga`);
  const analiticsData = await analitics.json();

  return {
    props: {
      about: response,
      footer: footerData,
      phone: contactData?.phone ?? {},
      seo: seoData ?? {},
      testimonials: testimonialsData ?? [],
      analitics: analiticsData ?? {},
    },
    revalidate: 23400,
  };
}
