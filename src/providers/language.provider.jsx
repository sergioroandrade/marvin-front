import { languageList } from 'locale';
import React, { createContext, useContext, useEffect, useState } from 'react';
export const LanguageContext = createContext({});

export function useLanguage() {
  return useContext(LanguageContext);
}

export function LanguageProvider({ children }) {
  const [language, setLanguage] = useState('pt-BR');
  const [dictionary, setDictionary] = useState({});

  useEffect(() => {
    const currentLanguage = localStorage.getItem('language');
    if (!currentLanguage) return;
    setLanguage(currentLanguage);
  }, []);

  useEffect(() => {
    const languageName = language === 'pt-BR' ? 'pt' : 'en';
    setDictionary(languageList[languageName]);
  }, [language]);

  function t(text) {
    return dictionary[text] || text;
  }

  return (
    <LanguageContext.Provider value={{ language, setLanguage, t }}>
      {children}
    </LanguageContext.Provider>
  );
}
