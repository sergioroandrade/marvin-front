import React, { createContext, useContext, useState } from 'react';
export const LoadingContext = createContext({});

export function useLoading() {
  return useContext(LoadingContext);
}

export function LoadingProvider({ children }) {
  const [loaderVisible, setLoaderVisible] = useState(false);

  return (
    <LoadingContext.Provider value={{ loaderVisible, setLoaderVisible }}>
      {children}
    </LoadingContext.Provider>
  );
}
